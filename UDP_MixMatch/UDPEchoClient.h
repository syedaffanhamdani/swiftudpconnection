#import <Foundation/Foundation.h>

@interface UDPEchoClient : NSObject
@property (nonatomic, copy, readwrite) NSData *          hostAddress;
@property (nonatomic, assign, readwrite) NSUInteger             port;
- (void) sendDataToHost:(NSString *)hostName port:(NSUInteger)port;
- (BOOL) sendData:(const char *)msg;
- (instancetype) initWithHostIP:(NSString *)hostIP port:(NSUInteger)port;

@end
