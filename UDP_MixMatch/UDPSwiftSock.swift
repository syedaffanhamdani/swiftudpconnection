//
//  UDPSwiftSock.swift
//  UDP_MixMatch
//
//  Created by Syed Affan Hamdani on 03.05.18.
//  Copyright © 2018 Syed Affan Hamdani. All rights reserved.
//

import Foundation
import CoreFoundation

func dataAvailableCallback(socket: CFSocket!, type: CFSocketCallBackType, address: CFData!, data: UnsafeRawPointer!, info: UnsafeMutableRawPointer!)
{
    // Accept connection and stuff later
}

class EchoServer : NSObject, StreamDelegate
{
    private var serverSocket: CFSocket?
    
    func start()
    {
        
        /*
         //  instantiating the CFSocketRef
         //
         cfsocketout = CFSocketCreate(kCFAllocatorDefault,
         PF_INET,
         SOCK_DGRAM,
         IPPROTO_UDP,
         kCFSocketDataCallBack,
         dataAvailableCallback,
         NULL);
         
         */
        self.serverSocket = CFSocketCreate(
                                kCFAllocatorDefault,
                                PF_INET,
                                SOCK_DGRAM,
                                IPPROTO_UDP,
                                3,
                                dataAvailableCallback, nil)
        //kCFSocketDataCallBack = 3
    }
}
