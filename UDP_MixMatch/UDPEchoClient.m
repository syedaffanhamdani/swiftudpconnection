#import "UDPEchoClient.h"

#import <CoreFoundation/CoreFoundation.h>
#import <sys/socket.h>
#import <arpa/inet.h>
#import <netinet/in.h>

#define IP      "172.20.10.3"
#define PORT    2222



static void dataAvailableCallback(CFSocketRef s, CFSocketCallBackType type, CFDataRef address, const void *data, void *info) {
    //
    //  receiving information sent back from the echo server
    //
    CFDataRef dataRef = (CFDataRef)data;
    NSLog(@"data recieved (%s) ", CFDataGetBytePtr(dataRef));
}

@implementation UDPEchoClient
{

    //
    //  socket for communication
    //
    CFSocketRef cfsocketout;
    
    //
    //  address object to store the host details
    //
    struct sockaddr_in  addr;
}

//TODO find out what this means
@synthesize hostAddress = _hostAddress;
@synthesize port        = _port;

/*

- (instancetype) init
{
    //Mark TODO check if the ip and ports are not set then set them to the defualt values
    self = [super init];
    if (self) {
        
        //
        //  instantiating the CFSocketRef
        //
        cfsocketout = CFSocketCreate(kCFAllocatorDefault,
                                     PF_INET,
                                     SOCK_DGRAM,
                                     IPPROTO_UDP,
                                     kCFSocketDataCallBack,
                                     dataAvailableCallback,
                                     NULL);
        
        memset(&addr, 0, sizeof(addr));
        
        addr.sin_len            = sizeof(addr);
        addr.sin_family         = AF_INET;
        addr.sin_port           = htons(PORT);
        addr.sin_addr.s_addr    = inet_addr(IP);
        
        //
        // set runloop for data reciever
        //
        CFRunLoopSourceRef rls = CFSocketCreateRunLoopSource(kCFAllocatorDefault, cfsocketout, 0);
        CFRunLoopAddSource(CFRunLoopGetCurrent(), rls, kCFRunLoopCommonModes);
        CFRelease(rls);
        
    }
    return self;
    
}
 */
- (instancetype) initWithHostIP:(NSString *)hostIP port:(NSUInteger)port
{
    //Mark TODO check if the ip and ports are not set then set them to the defualt values
    self = [super init];
    if (self) {
        
        //
        //  instantiating the CFSocketRef
        //
        cfsocketout = CFSocketCreate(kCFAllocatorDefault,
                                     PF_INET,
                                     SOCK_DGRAM,
                                     IPPROTO_UDP,
                                     kCFSocketDataCallBack,
                                     dataAvailableCallback,
                                     NULL);
        
        memset(&addr, 0, sizeof(addr));
        const char *cHostAddress = [hostIP cStringUsingEncoding:NSASCIIStringEncoding];
        addr.sin_len            = sizeof(addr);
        addr.sin_family         = AF_INET;
        addr.sin_port           = htons(port);
        addr.sin_addr.s_addr    = inet_addr(cHostAddress);
        
        //
        // set runloop for data reciever
        //
        CFRunLoopSourceRef rls = CFSocketCreateRunLoopSource(kCFAllocatorDefault, cfsocketout, 0);
        CFRunLoopAddSource(CFRunLoopGetCurrent(), rls, kCFRunLoopCommonModes);
        CFRelease(rls);
        
    }
    return self;
    
}


- (BOOL) sendData:(const char *)msg
{
    //
    //  checking, is my socket is valid
    //
    if(cfsocketout)
    {
        //
        //  making the data from the address
        //
        CFDataRef addr_data = CFDataCreate(NULL, (const UInt8*)&addr, sizeof(addr));
        
        //
        //  making the data from the message
        //
        CFDataRef msg_data  = CFDataCreate(NULL, (const UInt8*)msg, strlen(msg));
        NSLog(@"strlen(msg) = %lu",strlen(msg));
        
        
        //
        //  actually sending the data & catch the status
        //
        CFSocketError socketErr = CFSocketSendData(cfsocketout,
                                                   addr_data,
                                                   msg_data,
                                                   0);
        
        //
        //  return true/false upon return value of the send function
        //
        return (socketErr == kCFSocketSuccess);
        
    }
    else
    {
        NSLog(@"socket reference is null");
        return false;
    }
}

- (void) sendDataToHost:(NSString *)hostName port:(NSUInteger)port
// See comment in header.
{
    NSLog(@"\n Tying to send data");
    
    NSLog(@"host name:%@ port number:%lu", hostName,port);
}
@end
